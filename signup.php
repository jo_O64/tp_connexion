<?php 
session_start();
$erreurMail = false;
$pbMdp = false;
$pbChamp = false;
$mailInvalid = false;
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (!empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['password2']) && !empty($_POST['pseudo'])) {
        $mailValid = true;
        if (!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL) ) {
            $mailValid = false;
            $mailInvalid = '<div class="alert alert-danger" role="alert">
            Format email incorrect!
          </div>';
        }
        foreach ($_SESSION["listUser"] as $value) {
            if ($value['email'] == $_POST['email']) {
                $mailValid = false;
                $erreurMail = '<div class="alert alert-danger" role="alert">
                Ce mail exist deja!
              </div>';
                break;
            }
        }
        if ($mailValid) {
            if ($_POST['password'] == $_POST['password2']) {
                $passwordH = password_hash($_POST['password'],PASSWORD_DEFAULT);
                // $_POST['password'] = $passwordH;
                $_POST = [
                    "email" => $_POST['email'],
                    "password" => $passwordH,
                    "pseudo" => htmlspecialchars($_POST['pseudo'])
                ];
                $_SESSION["listUser"][] = $_POST;
                header('Location: ./index.php');
                // var_dump($_SESSION["listUser"]);
            } else {
                $pbMdp = '<div class="alert alert-danger" role="alert">
                Les mots de passe ne correspondent pas!
              </div>';
            }
        }  
    } else {
        $pbChamp = '<div class="alert alert-danger" role="alert">
        Vous devez remplir les champs!
      </div>';
    }
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Sign up</title>
</head>
<body>
<h1>Enregistrement</h1>
<form method="POST">
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Entrez votre adresse mail</label>
    <input type="text" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
  </div>
  <div class="mb-3">
    <label for="pseudo" class="form-label">Entrer votre Nom d'utilisteur</label>
    <input type="text" name="pseudo" class="form-control" id="pseudo">
  </div>
  <div class="mb-3">
    <label for="Password1" class="form-label">Entrez un mot de passe</label>
    <input type="password" name="password" class="form-control" id="exampleInputPassword1">
  </div>
  <div class="mb-3">
    <label for="Password2" class="form-label">Entrez le même mot de passe</label>
    <input type="password" name="password2" class="form-control" id="exampleInputPassword1">
  </div>
  <button type="submit" class="btn btn-primary">Enregistrement</button>
  <?php 
  if ($erreurMail) {
      echo $erreurMail;
  } 
  if ($pbMdp) {
      echo $pbMdp;
  }
  if ($pbChamp) {
      echo $pbChamp;
  }
  if ($mailInvalid) {
      echo $mailInvalid;
  }
  ?>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>   
</body>
</html>