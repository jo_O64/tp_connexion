<?php
session_start();
if (!isset($_SESSION["auth"])) {
    $_SESSION["auth"] = false;
}
if (isset($_GET['deco'])) {
  $_SESSION["auth"] = false;
}
if ($_SESSION["auth"] == false) {
    header("Location: ./signin.php");
}
if (isset($_SESSION["listUser"])) {
    $tab = $_SESSION["listUser"];
} else {
    $tab = [
        [
            "email" => "mail@mail.fr",
            "password" => "password",
            "pseudo" => "pseudo"
        ],
        [
            "email" => "mail2@mail.fr",
            "password" => "password2",
            "pseudo" => "pseudo2"
        ]
    ];
    $_SESSION["listUser"] = $tab;
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/style.css">
    <title>Accueil</title>
</head>
<body>
    <?php 
    include("./nav.php");
    ?>
<h1>Bienvenue <?php echo $_SESSION['user']['pseudo']?></h1>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>