<?php 
$erreurVide = "";
$erreurIdent = "";
session_start();
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (!empty($_POST['email']) && !empty($_POST['password'])) {
      for ($i=0 ; $i < count($_SESSION["listUser"]); $i++) {
        $value = $_SESSION["listUser"][$i];
            if ($_POST["email"] == $value["email"] && password_verify($_POST['password'],$value["password"])) {
                $_SESSION["index"] = $i;
                $_SESSION['user'] = [
                          'email' => $value['email'],
                          'password' => $value['password'],
                          'pseudo' => $value['pseudo']
                ];
                $_SESSION["auth"]=true;
                header('Location: ./index.php');
            }else {
                $erreurIdent = '<div class="alert alert-danger" role="alert">
                Votre identifiant ou votre mot de passe sont incorrect!
              </div>';
            }   
        }
    } else {
        $erreurVide = '<div class="alert alert-danger" role="alert">
        Vous devez entrer une valeur!
        </div>'; 
    }
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Sign in</title>
</head>
<body>
    <h1>Connexion</h1>

    <form method="POST">
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Email address</label>
    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
   
    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Password</label>
    <input type="password" name="password" class="form-control" id="exampleInputPassword1">
  </div>
  <?php 
    if ($erreurIdent) {
        echo $erreurIdent;
    }
    ?>
  <div class="mb-3 form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Restez connécté</label>
  </div>
  <button type="submit" class="btn btn-primary">Connexion</button>
</form>
<?php 
    if ($erreurVide) {
        echo $erreurVide;
    }
    ?>
<p>Pas de compte ? <a href="./signup.php">Inscrivez vous</a></p>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>   
</body>
</html>