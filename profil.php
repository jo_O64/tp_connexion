<?php 
session_start();
$erreurVide = false;
$erreurMdp = false;
if (isset($_POST['modifier1'])) {
  if (!empty($_POST['email']) && !empty($_POST['pseudo']) && !empty($_POST['password'])) {
    if ($_POST['password'] == $_POST['password1']) {
      for ($i=0 ; $i < count($_SESSION["listUser"]); $i++) {
        if ($_SESSION["listUser"][$i]['email'] == $_SESSION["user"]['email']) {
          $_SESSION["user"] = [
            'email' => $_POST['email'],
            'password' => password_hash($_POST['password'],PASSWORD_DEFAULT),
            'pseudo' => $_POST['pseudo']
          ];
          $_SESSION["listUser"][$i] = $_SESSION["user"];
          header('Location: ./index.php');
        }
      }
    }else{
      $erreurMdp = '<div class="alert alert-danger" role="alert">
      Les mots de passe ne correspodent pas!
    </div>';
    }
  }else {
    $erreurVide = '<div class="alert alert-danger" role="alert">
    Vous devez remplir tous les champs pour modifier!
  </div>';
  }
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/style.css">
    <title>Profil</title>
</head>
<body>
<?php 
include("./nav.php");
?>
<h1>Profil de <?php echo $_SESSION['user']['pseudo']?> </h1>

<form method="POST">
  <div class="row">
    <div class="col mb-3">
      <label for="exampleInputEmail1" class="form-label ">Votre adresse mail</label>
      <input type="text" value=" <?php echo $_SESSION["user"]['email']; ?> "  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    </div>
    <div class="col mb-3">
      <label for="exampleInputEmail1" class="form-label">Votre nouvelle adresse mail</label>
      <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    </div>
  </div>
  <div class="row mb-3">
    <div class="col">
      <label for="exampleInputEmail1" class="form-label">Votre pseudo</label>
      <input type="text" value=" <?php echo $_SESSION["user"]['pseudo']; ?> "  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    </div>
    <div class="col mb-3">
      <label for="exampleInputEmail1" class="form-label">Votre nouveau pseudo</label>
      <input type="text" name="pseudo" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    </div>
  </div>
  <div class="row mb-3">
    <div class="col">
      <label for="exampleInputEmail1" class="form-label">Votre nouveau mot de passe</label>
      <input type="password" name="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    </div>
    <div class="col mb-3">
      <label for="exampleInputEmail1" class="form-label">Confirmer votre mot de passe</label>
      <input type="password" name="password1" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    </div>
  </div>
  <?php 
    if ($erreurMdp) {
      echo $erreurMdp;
    }
  ?>
  <form method="POST"><td><button type="submit" value= "<?php $i ?>" name="modifier1" class="btn btn-primary">modifier</button></td></form>
  <?php 
    if ($erreurVide) {
      echo $erreurVide;
    }
  ?>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>