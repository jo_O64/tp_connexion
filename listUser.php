<?php 
session_start();
if (isset($_GET['supprimer'])){
  array_splice($_SESSION["listUser"], $_GET['supprimer'], 1);
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/style.css">
    <title>Liste des utilisateurs</title>
</head>
<body> 
<?php 
include("./nav.php");
?>
<h1>Liste des utilisateurs</h1>
<table class="table table-striped w-75 mx-auto table table-bordered">
  <thead>
    <tr>
      <th scope="col" class="text-center">Mail</th>
      <th scope="col" class="text-center">Pseudo</th>
      <th scope="col" class="text-center">Action</th>
    </tr>
  </thead>
  <tbody>

<?php 
  for ($i=0 ; $i < count($_SESSION["listUser"]); $i++) {
      $value = $_SESSION["listUser"][$i];
      echo  '
      <tr>
          <td class="text-center">'.$value['email'].'</td>
          <td class="text-center">'.$value['pseudo'].'</td>
          <td class="text-center">'.'<button type="button" value="'.$i.'" name="supprimer1" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modal" data-bs-i='.$i.' data-bs-pseudo='.$value['pseudo'].'>Supprimer</button>'.'</td>
      ';
  }
?>
  </tbody>
</table>

<div class="modal fade" id="modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">test </h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <input type="text">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
        
        <form methode="GET"><button id="boutonSup" name='supprimer' type="submit" class="btn btn-primary">Confirmer</button></form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var exampleModal = document.getElementById('modal')
exampleModal.addEventListener('show.bs.modal', function (event) {
    // Bouton qui a déclenché le modal
    var button = event.relatedTarget
    // Extraire les informations des attributs data-bs-*
    var recipient = button.getAttribute('data-bs-i')
    var detail = button.getAttribute('data-bs-pseudo') 
    var modalTitle = exampleModal.querySelector('.modal-title')
    var modalBodyInput = exampleModal.querySelector('.modal-body')
    //On intègre un nouveau texte a notre paragraphe incluant le perso a supprimer
    modalTitle.textContent = 'Confirmation de suppression ' 
    modalBodyInput.textContent = 'Voulez vous vraiment supprimer ' + detail
    //On récupère dans une variable la balise ayant pour id 'boutonSup'
    var boutonSup = document.getElementById('boutonSup')
    //On attribue recipient a la valeur de butonSup
    boutonSup.value = recipient
    // $index =  document.write(recipient)
})
</script>  
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>